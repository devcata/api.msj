const express = require('express');
const app = express();         
const bodyParser = require('body-parser');
const port = 9000;
const mssql = require('mssql');
const cors = require('cors');

const db = {
    server: '10.10.0.4',
    port: 1320,
    database: 'TESTE',
    user: 'sa',
    pass: '102010'
};

// String Connection
const connStr = `Server=${db.server},${db.port};Database=${db.database};User Id=${db.user};Password=${db.pass}`;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Cors Setup - *All rules
app.use(cors());

const router = express.Router();
router.get('/', (req, res) => res.json({ message: `Servidor rodando na porta ${port}`}));

router.get('/recursos', (req, res) => {

    let query = `SELECT H1_FILIAL FILIAL, H1_CODIGO CODIGO, RTRIM(H1_DESCRI) DESCRICAO,`;
    query += ` H1_CCUSTO CCUSTO FROM SH1010 WHERE D_E_L_E_T_ <> '*' AND H1_FILIAL = '03'`;
    query += ` AND H1_CCUSTO IN ('3611', '3602')`;

    //res.json({sql: query});
    execSQLQuery(query, res);
});

router.get('/operadores', (req, res) => {

    let query = `SELECT RA_FILIAL FILIAL, RA_MAT MATRICULA, RTRIM(RA_NOME) NOME`;
    query += ` FROM SRA010 WHERE RA_SITFOLH IN('', 'F') AND D_E_L_E_T_ <> '*'`;
    query += ` AND RA_FILIAL = '03' ORDER BY RA_NOME ASC`;

    execSQLQuery(query, res);
});

router.get('/custos', (req, res) => {

    let query = `SELECT RTRIM(CTT_CUSTO) CCUSTO, RTRIM(CTT_DESC01) DESCRICAO`;
    query += ` FROM CTT010 WHERE D_E_L_E_T_ <> '*'`;
    
    execSQLQuery(query, res);
});

router.get('/motivos', (req, res) => {

    let query = `SELECT RTRIM(X5_FILIAL) FILIAL, RTRIM(ZAQ_CCUSTO) CCUSTO,`;
    query += ` RTRIM(X5_CHAVE) MOTIVO, RTRIM(X5_DESCRI) DESCRICAO`;
    query += ` FROM ZAQ010 ZAQ`;
    query += ` INNER JOIN SX5010 X5 ON`;
    query += ` X5_FILIAL = ZAQ_FILIAL AND ZAQ_MOTIVO = X5_CHAVE AND X5_TABELA = '44' AND X5.D_E_L_E_T_ <> '*' WHERE ZAQ_FILIAL = '03'`;
    //res.json({sql: query});
    execSQLQuery(query, res);
});

router.post('/eficiencia-msj', (req, res) =>{

    const table = 'TBAPPE';

    const dt_ini = req.body.data_inicial;
    const hor_ini = req.body.hora_inicial;
    const recurso = req.body.id_recurso;
    //const recurso = req.body.recurso;
    const ccusto = req.body.ccusto;
    const operador = req.body.id_operador;
    //const operador = req.body.operador;
    const medicao_inicial = parseFloat(req.body.medicao_inicial);
    const turma = req.body.turma;
    const usuario = req.body.usuario;
    const filial = '03';

    let query = `INSERT INTO ${table}`;
    query += ` (dt_ini, hr_ini, filial, recurso, medicao_ini, turma,`;
    query += ` ccusto, operador, usuario)`;
    
    query += ` VALUES ('${dt_ini}', '${hor_ini}', '${filial}', '${recurso}',`;
    query += ` ${medicao_inicial}, '${turma}', '${ccusto}', '${operador}', '${usuario}')`;

    console.log(query);

    execSQLQuery(query, res);
});

router.post('/improdutivo-msj', (req, res) =>{

    const table = 'TBAPPI';

    const filial = '03';
    const dt_ini = req.body.dataInicial;
    const dt_final = req.body.dataFinal;
    const hr_ini = req.body.horaInicial;
    const hr_final = req.body.horaFinal;
    const ccusto = req.body.ccusto;
    const motivo = req.body.idmotivo;
    const recurso = req.body.recurso;
    const usuario = req.body.usuario;
    

    let query = `INSERT INTO ${table}`;
    query += ` (filial, dt_ini, dt_final, hr_ini, hr_final,`;
    query += ` ccusto, motivo, recurso, usuario)`;
    
    query += ` VALUES ('${filial}', '${dt_ini}', '${dt_final}',`;
    query += ` '${hr_ini}', '${hr_final}', '${ccusto}', '${motivo}',`;
    query += ` '${recurso}', '${usuario}')`;

    console.log(query);

    execSQLQuery(query, res);
});

app.use('/', router);

// Conexão global
mssql.connect(connStr)
   .then(conn => {
        global.conn = conn;

        // Inicia o servidor
        app.listen(port, () => {
            console.log(`Servidor rodando na porta ${port}`);
            console.log('Pressione CTRL + C para encerrar a execução');
        });
   })
   .catch(err => console.log(err));

function execSQLQuery(sql, res){
    global.conn.request()
               .query(sql)
               .then(result => res.json(result.recordset))
               .catch(err => res.json(err));
}